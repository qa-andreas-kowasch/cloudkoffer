# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/clementblaise/age" {
  version     = "0.1.1"
  constraints = "~> 0.1.1"
  hashes = [
    "h1:4XI5E9xta7eDFNxk/LU50ZEMwpTsi/pDCRVCaQxkKwY=",
    "h1:TfefAE7jcX7fHjLd6T5DIY7N/r+MREVYXlV3EPCQWtU=",
    "zh:0809283e626d14964582fbc80edc7163e3a72b78033041d343ce563b7c211dac",
    "zh:1b5c3b125d22baab92ea8c65ef3650671dee19b17f5ccb1f09167228c6468fc9",
    "zh:25919262fba877f9e122e13b5273cc154be61105849eaa477c7fc1a4cfe2f5a4",
    "zh:28969f204e72957ad88037d2d878a57ea478640f0bf2f41768c28adafb24a53e",
    "zh:380b2f5d646d35f922af74105f06667ece1d93447cb5ab071806f0723b7bec36",
    "zh:420b2c0bf37d69ee426dd84701d23361b41bc5dbc86503220363758bebeeb6b1",
    "zh:4e15355e217f466b718ff911adefd7645fb86c256ced5fb04b1ba9f26888aea4",
    "zh:61318d662cbbc267af9644482fe379b20cd6b71c56f60e9c5f1b7667d265c20e",
    "zh:6e9384d0a5c48eaab63c9d4b6147c4d5cde3df01b51e75122b9221ffbc596ecf",
    "zh:73f5152603907ab0916628a3fc59c92b2da7c86d7ff34642c154085912bde085",
    "zh:bcc901212315373aaf2b7523b4208c3507d31a4838a0163241d29c1e2ba25a86",
    "zh:cf0ab650eaf17bd201552dfabe887d677678b2150e85a6fd11299a2020e6eb9a",
    "zh:dea911656fa62694c95e5476e5e05c277f34daf286b7b68e9a4977ad56db365e",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
    "zh:fe8afdf0fe5ade3f2474042f731798c516ad556e9f0cc18f4772c746469672c2",
  ]
}

provider "registry.terraform.io/fluxcd/flux" {
  version     = "1.0.0-rc.5"
  constraints = "1.0.0-rc.5"
  hashes = [
    "h1:L+F0Hr2xDYuwSSb1nYM39xR0a00dmZ3RjHKStjFOIqk=",
    "zh:02a371a4d39565526db795569f6aebe3452c18922bf59a448035aa81a61acba3",
    "zh:3b07bd8290469123977c716f764c0ea04e1d37e02fdfae4054dd8a00239258e9",
    "zh:4b699a3abf50e31424ed34fe6daab6350ddeb090c352e123801fa6fdd9a8a304",
    "zh:50858f5c380c4da570662cc4210e5877ca5ee6f218bd9681b84c7da0caa552be",
    "zh:5b1baa8beac4d42ed31b3c8a0481d7a6707c2fa210caef7fbe9be6bc67f1bcb7",
    "zh:68be107ec64295e3154a97443c6c599faa5be8ddd989671d4e6c206e22c9e642",
    "zh:68c6f8a7189388d58e9931309c82e5bc3d8ba8f59607c0da7f07e582aa1d06c9",
    "zh:72e1e301f635c6c3746274a5b17082cb1fdc2de626e7377ee9ba3cf061da3ed6",
    "zh:9afaec0971fa3e2c088e592731ff8566bfdf4646ec8ee3c3b6b917780cf3fd4c",
    "zh:ab492201c5075cb3cfc6e1a6bdcd8ed5b4c5452d16bf1067ff759bd140e41c8b",
    "zh:b5a47a72856d0cef9a626e6d9f703d9ae4c3777e8cc2183ec61f6e230d8d684e",
    "zh:c945b236a47eefdd48c1c1c00c028ab6c0a3c1ac1d04d088a7d4ced1ebbe5ff2",
    "zh:e610afdc5e22aa57fe5da77262d756baa13b20ace63952c53c45abacfcb78402",
    "zh:e8988222b607acd5a866d55bbbc9e79fdf7120efd142ede50ad16cf8c38bc205",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version     = "2.21.1"
  constraints = "~> 2.21.1"
  hashes = [
    "h1:2spGoBcGDQ/Csc23bddCfM21zyKx3PONoiqRgmuChnM=",
    "zh:156a437d7edd6813e9cb7bdff16ebce28cec08b07ba1b0f5e9cec029a217bc27",
    "zh:1a21c255d8099e303560e252579c54e99b5f24f2efde772c7e39502c62472605",
    "zh:27b2021f86e5eaf6b9ee7c77d7a9e32bc496e59dd0808fb15a5687879736acf6",
    "zh:31fa284c1c873a85c3b5cfc26cf7e7214d27b3b8ba7ea5134ab7d53800894c42",
    "zh:4be9cc1654e994229c0d598f4e07487fc8b513337de9719d79b45ce07fc4e123",
    "zh:5f684ed161f54213a1414ac71b3971a527c3a6bfbaaf687a7c8cc39dcd68c512",
    "zh:6d58f1832665c256afb68110c99c8112926406ae0b64dd5f250c2954fc26928e",
    "zh:9dadfa4a019d1e90decb1fab14278ee2dbefd42e8f58fe7fa567a9bf51b01e0e",
    "zh:a68ce7208a1ef4502528efb8ce9f774db56c421dcaccd3eb10ae68f1324a6963",
    "zh:acdd5b45a7e80bc9d254ad0c2f9cb4715104117425f0d22409685909a790a6dd",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
    "zh:fb451e882118fe92e1cb2e60ac2d77592f5f7282b3608b878b5bdc38bbe4fd5b",
  ]
}

provider "registry.terraform.io/hashicorp/local" {
  version     = "2.4.0"
  constraints = "~> 2.4.0"
  hashes = [
    "h1:Bs7LAkV/iQTLv72j+cTMrvx2U3KyXrcVHaGbdns1NcE=",
    "h1:ZUEYUmm2t4vxwzxy1BvN1wL6SDWrDxfH7pxtzX8c6d0=",
    "zh:53604cd29cb92538668fe09565c739358dc53ca56f9f11312b9d7de81e48fab9",
    "zh:66a46e9c508716a1c98efbf793092f03d50049fa4a83cd6b2251e9a06aca2acf",
    "zh:70a6f6a852dd83768d0778ce9817d81d4b3f073fab8fa570bff92dcb0824f732",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:82a803f2f484c8b766e2e9c32343e9c89b91997b9f8d2697f9f3837f62926b35",
    "zh:9708a4e40d6cc4b8afd1352e5186e6e1502f6ae599867c120967aebe9d90ed04",
    "zh:973f65ce0d67c585f4ec250c1e634c9b22d9c4288b484ee2a871d7fa1e317406",
    "zh:c8fa0f98f9316e4cfef082aa9b785ba16e36ff754d6aba8b456dab9500e671c6",
    "zh:cfa5342a5f5188b20db246c73ac823918c189468e1382cb3c48a9c0c08fc5bf7",
    "zh:e0e2b477c7e899c63b06b38cd8684a893d834d6d0b5e9b033cedc06dd7ffe9e2",
    "zh:f62d7d05ea1ee566f732505200ab38d94315a4add27947a60afa29860822d3fc",
    "zh:fa7ce69dde358e172bd719014ad637634bbdabc49363104f4fca759b4b73f2ce",
  ]
}

provider "registry.terraform.io/hashicorp/tls" {
  version     = "4.0.4"
  constraints = "~> 4.0.4"
  hashes = [
    "h1:GZcFizg5ZT2VrpwvxGBHQ/hO9r6g0vYdQqx3bFD3anY=",
    "h1:Wd3RqmQW60k2QWPN4sK5CtjGuO1d+CRNXgC+D4rKtXc=",
    "zh:23671ed83e1fcf79745534841e10291bbf34046b27d6e68a5d0aab77206f4a55",
    "zh:45292421211ffd9e8e3eb3655677700e3c5047f71d8f7650d2ce30242335f848",
    "zh:59fedb519f4433c0fdb1d58b27c210b27415fddd0cd73c5312530b4309c088be",
    "zh:5a8eec2409a9ff7cd0758a9d818c74bcba92a240e6c5e54b99df68fff312bbd5",
    "zh:5e6a4b39f3171f53292ab88058a59e64825f2b842760a4869e64dc1dc093d1fe",
    "zh:810547d0bf9311d21c81cc306126d3547e7bd3f194fc295836acf164b9f8424e",
    "zh:824a5f3617624243bed0259d7dd37d76017097dc3193dac669be342b90b2ab48",
    "zh:9361ccc7048be5dcbc2fafe2d8216939765b3160bd52734f7a9fd917a39ecbd8",
    "zh:aa02ea625aaf672e649296bce7580f62d724268189fe9ad7c1b36bb0fa12fa60",
    "zh:c71b4cd40d6ec7815dfeefd57d88bc592c0c42f5e5858dcc88245d371b4b8b1e",
    "zh:dabcd52f36b43d250a3d71ad7abfa07b5622c69068d989e60b79b2bb4f220316",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
  ]
}

provider "registry.terraform.io/integrations/github" {
  version     = "5.26.0"
  constraints = "~> 5.26.0"
  hashes = [
    "h1:LTSeUuYyaR/Y85nbCTrBa62jTJWC3bCQ6MLzGU1aQGw=",
    "zh:09236b5396ec086b3d6c06cfe4160185ef0364d2f99deea00576d3ad5864db49",
    "zh:1335b570cc3ddade75bac3fe691f26d159b5d2cfc1a96a23bdef6a6dbf86b090",
    "zh:157659e5f0d43109d0ac523c3e3abdd35463fef089dab72ecb291314a26e7731",
    "zh:235aab664e2633ad597a0add45547591bc265610b80c554903b76f6094b5ffc9",
    "zh:4a40582adf31bacddd37ed24d6224adc859bdddf9d8b9800a3da33897d23fe40",
    "zh:4b0ae10366e57e40221a83135d5a918d8e0bc99fcc94ee4ceb045257ac8e5871",
    "zh:5b7046eb963572ac7f2c1e2f3fb831645206c931c94fa465e24975c5f6c2441a",
    "zh:5f362404ecbbde9fd087e0fdf766a619319b666dd4118116b946fd32b11bb62c",
    "zh:75ffafe88fa3ae1117c57aca8237ca44ef871956484b7924d0114907a2303e15",
    "zh:99ae45c65008d545f79cdecd18c17fc2a58e6f392691afd7ce9cbdda2cd63622",
    "zh:bd1ac24942aba812866f1240bf5a0c2ec075dae7c564146bcbf29bac4eaaf4d1",
    "zh:d5ccd9e2dc2ef2d13ffd884343b7a3972ee5ba3986f72d65c577c32fc460af34",
    "zh:ef87cd75e01df472a44922a8a4ff03d7d759fa5d2aebe61f9b2f05c796734d1b",
    "zh:ff3d11577c594d244015d5a708b7cb2c91058c3ad075c3e78c6f8b9fb07e8244",
  ]
}
