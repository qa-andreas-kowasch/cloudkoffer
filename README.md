# Cloudkoffer

## Architecture

``` plantuml
@startuml architecture

cloud WAN #A7E8E0
cloud LAN1 #A7E8E0
cloud LAN2 #A7E8E0
agent router #A0C5D3 [
  <b>router</b>
  (dhcp, dns, pxe/tftp)
]
agent switch #A0C5D3
frame controlplane #F7EAC9 {
  agent "node-3" as node_3 #F6CBC0
  agent "node-2" as node_2 #F6CBC0
  agent "node-1" as node_1 #F6CBC0
}
frame worker #F7EAC9 {
  collections "node-..." as nodes #F6CBC0
}

WAN -- router
LAN1 -- router
LAN2 -- router
router -- switch
switch -- node_1
switch -- node_2
switch -- node_3
switch -- nodes

@enduml
```

## Repository Layout

- **hardware** - Contains information about the configuration of the cases, routers, switches, nodes, clients and showcases.

- **deployment-talos** - Contains the steps necessary to create a Talos cluster and deploy it to the nodes.
  - manual
  - terraform (`recommended`)

- **deployment-flux** - Contains the steps necessary to bootstrap flux into the cluster.
  - manual
  - terraform (`recommended`)

- **deployment-stack** - Contains the steps necessary to build the infrastructure stack.

## Building a Cluster

- Boot Talos in maintenence mode.

  - **Keyboard F12**
    - Ensure the nodes are shutdown.
    - Connect a keyboard to a node and press its power button.
    - Press F12 (repeatedly) during the boot process to trigger the network boot.
  - **Local Medium**
    - Prepare bootable USB flash drive or SD card (e.g. [balenaEtcher](https://www.balena.io/etcher)) using `talos-amd64.ios` from Talos GitHub [release page](https://github.com/siderolabs/talos/releases).
    - Ensure the nodes are shutdown.
    - Connect the local medium to a node and press its power button.
    - Select `Reset Talos installation` if Talos was previously installed, otherwise `Talos ISO`.

- Apply configuration in `deployment-talos`.
- Apply configuration in `deployment-flux`.
- Apply configuration in `deployment-stack`.

## Talos

[Talos Linux](https://www.talos.dev) is Linux designed for Kubernetes – secure, immutable, and minimal.

- Supports cloud platforms, bare metal, and virtualization platforms
- All system management is done via an API. No SSH, shell or console
- Production ready: supports some of the largest Kubernetes clusters in the world
- Open source project from the team at Sidero Labs

### Why Talos Linux?

- **Security** - Talos reduces your attack surface. It's minimal, hardened and immutable. All API access is secured with mutual TLS (mTLS) authentication.
- **Predictability** - Talos eliminates configuration drift, reduces unknown factors by employing immutable infrastructure ideology, and delivers atomic updates.
- **Evolvability** - Talos simplifies your architecture, increases your agility, and always delivers current stable Kubernetes and Linux versions.

### Features

- **Minimal** - Talos consists of only a handful of binaries and shared libraries: just enough to run containerd and a small set of system services. This aligns with NIST's recommendation in the Application Container Security Guide.
- **Hardened** - Built with the Kernel Self Protection Project configuration recommendations. All access to the API is secured with Mutual TLS. Settings and configuration described in the CIS guidelines are applied by default.
- **Immutable** - Talos improves security further by mounting the root filesystem as read-only and removing any host-level such as a shell and SSH.
- **Ephemeral** - Talos runs in memory from a SquashFS, and persists nothing, leaving the primary disk entirely to Kubernetes.
- **Current** - Delivers the latest stable versions of Kubernetes and Linux.
