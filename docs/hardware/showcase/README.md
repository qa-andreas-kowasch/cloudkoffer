# Talos / Hardware / Showcase

## How do I connect the components?

- Connect Cloudkoffer to power source (with power limiter).
- Connect Internet to Cloudkoffer `WAN` port.
- Connect iMac to power source.
- Connect iMac to Cloudkoffer `LAN1` or `LAN2` port.
- Connect kubepad to iMac USB port.

## How do I start the cluster?

- Press all three power buttons on the backside of the Cloudkoffer.
- Wait until the router has started (~ 1-2 minutes).
- Press power button on each node.
- Wait until all pods are ready (~ 2-5 minutes).

## How do I start the iMac?

- Press power button on the backside.
- Wait until macOS has started.
- Login:
  - Username: `messebmwshowcase`
  - Password: `MesseMac2023!`

## How do I start the kubepad (Novation Launchpad MIDI controller)?

``` shell
cd ~/Documents/kubepad
./gradlew run
```

<!--
If an error occurs, ensure the following libs are included in the `build.gradle` file.

``` gradle
compile 'org.bouncycastle:bcpkix-jdk18on:1.72'
compile 'org.bouncycastle:bcprov-jdk18on:1.72'
compile 'org.bouncycastle:bcprov-ext-jdk18on:1.72'
```
-->

## How do I start the cloudcontrol (Novation Launch Control MIDI controller)?

``` shell
cd ~/Documents/cloudcontrol
./gradlew run
```

<!--
If an error occurs, ensure the following libs are included in the `build.gradle` file.

``` gradle
compile 'org.bouncycastle:bcpkix-jdk18on:1.72'
compile 'org.bouncycastle:bcprov-jdk18on:1.72'
compile 'org.bouncycastle:bcprov-ext-jdk18on:1.72'
```
-->

## How do I shutdown the iMac?

- Shutdown macOS or press power button on the backside.

## How do I shutdown the cluster?

- Press power button on each node.
- Wait until all nodes are down (~ 2-5 minutes).
- Press all three power buttons on the backside of the Cloudkoffer.

## What are the components of the cluster?

- Nodes: 10x Intel NUC Kit NUC6i7KYB
  - Intel Core i7-6770HQ (2.60 - 3.50 GHz, 6 MB L3 Cache, 4 Cores / 8 Threads)
  - 32 GB Memory (DDR4-2133)
  - Intel Iris Pro Graphics 580 (Mini-DP 1.2, HDMI 2.0, USB-C DP1.2)
  - Samsung SSD 960 PRO 1 TB
- Router: Ubiquiti EdgeRouter X (ER-X)
- Switch: Netgear ProSafe GS116Ev2 (managed)

## What software components are installed on the cluster?

### Management

Component | Description
--- | ---
Talos | Operating System
Flux | GitOps Tool

### Connectivity

Component | Description
--- | ---
Cilium | Container Network Interface
CoreDNS | Internal DNS Server
MetalLB | Loadbalancer
Ingress NGINX | Ingress Controller
Cert Manager | Certificate Controller
External DNS | External DNS Configurator

### Observability

Component | Description
--- | ---
Kubernetes Dashboard | Kubernetes Web UI
Weave GitOps | Flux Web UI
Hubble | Network, Service & Security Observability
Prometheus | Metrics Collector
Prometheus Node Exporter | Metric Exporter (Hardware, Kernel)
Kube State Metrics | Metric Exporter (K8s Objects)
Alertmanager | Alerting System
Grafana | Metrics Logs Traces Monitoring
Loki | Log Aggregator
Promtail | Log Shipper
Tempo | Tracing Backend
Kubernetes Event Exporter | Event Exporter
Vertical Pod Autoscaler | Resource Management
Goldilocks | Resource Management

### Security

Component | Description
--- | ---
Kyverno | Policy Engine
Trivy Operator | Security Toolkit
Sealed Secrets | Secrets Operator
External Secrets | Secrets Operator

## Which web applications are available on the cluster?

- [Kubernetes Dashboard](https://kubernetes-dashboard.ck3.cluster.qaware.de)
- [Weave GitOps](https://weave-gitops.ck3.cluster.qaware.de) (Username: `admin`, Password: `QAware`)
- [Hubble](hubble.ck3.cluster.qaware.de)
- [Prometheus](https://prometheus.ck3.cluster.qaware.de)
- [Alertmanager](https://alertmanager.ck3.cluster.qaware.de)
- [Grafana](https://grafana.ck3.cluster.qaware.de) (Username: `admin`, Password: `QAware`)
- [Goldilocks](https://goldilocks.ck3.cluster.qaware.de)
- [Router](https://192.168.1.254)
- [Switch](https://192.168.1.253)

## Which GUI applications are available on the iMac?

- 1password
- 1password-cli
- balenaetcher
- caffeine
- firefox
- google-chrome
- google-cloud-sdk
- intellij-idea
- iterm2
- keepassxc
- lens
- macfuse
- postman
- rectangle
- rapidapi
- slack
- visual-studio-code
- zoom

## Which CLI applications are available on the iMac?

- age
- bat
- checkov
- cilium-cli
- colima
- curl
- doitlive
- flux
- fzf
- gettext
- gh
- git
- gitops
- glab
- go
- hadolint
- helm
- helm-docs
- jq
- jsonschema
- k9s
- kdash
- krew
- kube-score
- kubeconform
- kubernetes-cli
- kubeseal
- kubeval
- kustomize
- mc
- pre-commit
- sdkman-cli
- shellcheck
- sops
- step
- stern
- terraform
- terraform-docs
- tflint
- tfsec
- trivy
- watch
- wget
- yamllint
- yq
- zsh
