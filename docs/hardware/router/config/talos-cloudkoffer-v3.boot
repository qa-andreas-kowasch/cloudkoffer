firewall {
    all-ping enable
    broadcast-ping disable
    ipv6-name WANv6_IN {
        default-action drop
        description "WAN inbound traffic forwarded to LAN"
        enable-default-log
        rule 10 {
            action accept
            description "Allow established/related sessions"
            state {
                established enable
                related enable
            }
        }
        rule 20 {
            action drop
            description "Drop invalid state"
            state {
                invalid enable
            }
        }
    }
    ipv6-name WANv6_LOCAL {
        default-action drop
        description "WAN inbound traffic to the router"
        enable-default-log
        rule 10 {
            action accept
            description "Allow established/related sessions"
            state {
                established enable
                related enable
            }
        }
        rule 20 {
            action drop
            description "Drop invalid state"
            state {
                invalid enable
            }
        }
        rule 30 {
            action accept
            description "Allow IPv6 icmp"
            protocol ipv6-icmp
        }
        rule 40 {
            action accept
            description "allow dhcpv6"
            destination {
                port 546
            }
            protocol udp
            source {
                port 547
            }
        }
    }
    ipv6-receive-redirects disable
    ipv6-src-route disable
    ip-src-route disable
    log-martians enable
    name WAN_IN {
        default-action drop
        description "WAN to internal"
        rule 10 {
            action accept
            description "Allow established/related"
            state {
                established enable
                related enable
            }
        }
        rule 20 {
            action drop
            description "Drop invalid state"
            state {
                invalid enable
            }
        }
    }
    name WAN_LOCAL {
        default-action drop
        description "WAN to router"
        rule 10 {
            action accept
            description "Allow established/related"
            state {
                established enable
                related enable
            }
        }
        rule 20 {
            action drop
            description "Drop invalid state"
            state {
                invalid enable
            }
        }
    }
    receive-redirects disable
    send-redirects enable
    source-validation disable
    syn-cookies enable
}
interfaces {
    ethernet eth0 {
        address dhcp
        description Internet
        duplex auto
        firewall {
            in {
                ipv6-name WANv6_IN
                name WAN_IN
            }
            local {
                ipv6-name WANv6_LOCAL
                name WAN_LOCAL
            }
        }
        speed auto
    }
    ethernet eth1 {
        description Local
        duplex auto
        speed auto
    }
    ethernet eth2 {
        description Local
        duplex auto
        speed auto
    }
    ethernet eth3 {
        description Local
        duplex auto
        speed auto
    }
    ethernet eth4 {
        description Local
        duplex auto
        poe {
            output off
        }
        speed auto
    }
    loopback lo {
    }
    switch switch0 {
        address 192.168.1.254/24
        description Local
        mtu 1500
        switch-port {
            interface eth1 {
            }
            interface eth2 {
            }
            interface eth3 {
            }
            interface eth4 {
            }
            vlan-aware disable
        }
    }
}
service {
    dhcp-server {
        disabled false
        hostfile-update disable
        shared-network-name LAN {
            authoritative enable
            subnet 192.168.1.0/24 {
                default-router 192.168.1.254
                dns-server 192.168.1.254
                domain-name case.local
                lease 86400
                start 192.168.1.51 {
                    stop 192.168.1.100
                }
                static-mapping node-1 {
                    ip-address 192.168.1.1
                    mac-address 00:1f:c6:9c:8b:2d
                }
                static-mapping node-2 {
                    ip-address 192.168.1.2
                    mac-address 00:1f:c6:9c:86:8f
                }
                static-mapping node-3 {
                    ip-address 192.168.1.3
                    mac-address 00:1f:c6:9d:09:08
                }
                static-mapping node-4 {
                    ip-address 192.168.1.4
                    mac-address 00:1f:c6:9c:8a:20
                }
                static-mapping node-5 {
                    ip-address 192.168.1.5
                    mac-address 00:1f:c6:9c:89:ca
                }
                static-mapping node-6 {
                    ip-address 192.168.1.6
                    mac-address 00:1f:c6:9c:90:a7
                }
                static-mapping node-7 {
                    ip-address 192.168.1.7
                    mac-address 00:1f:c6:9c:89:d7
                }
                static-mapping node-8 {
                    ip-address 192.168.1.8
                    mac-address 00:1f:c6:9c:8f:f2
                }
                static-mapping node-9 {
                    ip-address 192.168.1.9
                    mac-address 00:1f:c6:9c:92:a0
                }
                static-mapping node-10 {
                    ip-address 192.168.1.10
                    mac-address 00:1f:c6:9c:91:e6
                }
                static-mapping switch {
                    ip-address 192.168.1.253
                    mac-address a0:04:60:08:f7:9f
                }
            }
        }
        static-arp disable
        use-dnsmasq enable
    }
    dns {
        forwarding {
            cache-size 1000
            listen-on switch0
            name-server 8.8.8.8
            name-server 1.1.1.1
            name-server 9.9.9.9
            options enable-tftp
            options tftp-root=/var/lib/tftpboot
            options dhcp-boot=ipxe.efi
            options dhcp-userclass=set:ipxe,iPXE
            options dhcp-boot=tag:ipxe,boot.ipxe
            options "dhcp-option=encap:175, 176, 1b"
        }
    }
    gui {
        http-port 80
        https-port 443
        older-ciphers enable
    }
    nat {
        rule 5010 {
            description "masquerade for WAN"
            outbound-interface eth0
            type masquerade
        }
    }
    ssh {
        port 22
        protocol-version v2
    }
    unms {
        disable
    }
}
system {
    analytics-handler {
        send-analytics-report false
    }
    crash-handler {
        send-crash-report false
    }
    ipv6 {
    }
    login {
        user ubnt {
            authentication {
                encrypted-password ****************
            }
            level admin
        }
    }
    ntp {
        server 0.ubnt.pool.ntp.org {
        }
        server 1.ubnt.pool.ntp.org {
        }
        server 2.ubnt.pool.ntp.org {
        }
        server 3.ubnt.pool.ntp.org {
        }
    }
    static-host-mapping {
        host-name kube.case.local {
            alias kube
            inet 192.168.1.101
        }
        host-name router.case.local {
            alias router
            inet 192.168.1.254
        }
    }
    syslog {
        global {
            facility all {
                level notice
            }
            facility protocols {
                level debug
            }
        }
    }
    time-zone UTC
}
