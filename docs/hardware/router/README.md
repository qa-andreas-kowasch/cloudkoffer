
<!-- markdownlint-disable MD033 -->
# Talos / Hardware / Router

![EdgeRouter X](img/EdgeRouter-X.jpg "EdgeRouter X")

- **Type**: Ubiquiti EdgeRouter X (ER-X)
- **URL**: [192.168.1.254](https://192.168.1.254)
- **Username**: ubnt
- **Password**: ubnt

Port | Connection
--- | ---
`eth0` | WAN
`eth1` | LAN1 (`cloudkoffer-v2` / `v3`)
`eth2` | LAN2 (`cloudkoffer-v3`)
`eth3` | -
`eth4` | Switch

## Functionality

### DHCP, DNS and PXE/TFTP Server

Property | Value
--- | ---
**Subnet** | 192.168.1.0/24
**Dynamic DHCP Range** | 192.168.1.51 - 192.168.1.100
**Static DCHP Range** | 192.168.1.1 - 192.168.1.50
**Router** | 192.168.1.254
**DNS Server** | 192.168.1.254
**Boot Server** | 192.168.1.254
**Domain Name** | case.local
**Host Mapping**<br />`cloudkoffer-v1` / `v2` / `v3` | kube: 192.168.1.101<br />node1: 192.168.1.1<br />node2: 192.168.1.2<br />node3: 192.168.1.3<br />node4: 192.168.1.4<br />node5: 192.168.1.5
**Host Mapping**<br />`cloudkoffer-v2` / `v3` | node6: 192.168.1.6<br />node7: 192.168.1.7<br />node8: 192.168.1.8<br />node9: 192.168.1.9<br />node10: 192.168.1.10
**Host Mapping**<br />`cloudkoffer-v3` | switch: 192.168.1.253
**Upstream DNS Server** | 8.8.8.8, 1.1.1.1, 9.9.9.9

### Network Boot

State Diagram

``` plantuml
@startuml network-boot-state

hide empty description

state "UEFI" as uefi
state "PXE" as pxe
state "iPXE" as ipxe
state "Image" as image

[*] -> uefi
uefi -> pxe
pxe -> ipxe
ipxe -> image
image -> [*]

@enduml
```

Sequence Diagram

``` plantuml
@startuml network-boot-sequence

box "Node" #F6CBC0
participant "UEFI" as uefi #FFF
participant "PXE" as pxe #FFF
participant "iPXE" as ipxe #FFF
participant "Image" as image #FFF
end box

box "Router" #A0C5D3
participant "DHCP" as dhcp #FFF
participant "TFTP" as tftp #FFF
end box

uefi -> pxe : Load PXE

activate pxe #E76F51

group Request iPXE Firmware
pxe -> dhcp : DHCP Request
activate dhcp #264653
dhcp -> pxe : DHCP Reply\n* Bootfile Server: ""router""\n* Bootfile Name: <back:#2A9D8F> ""ipxe.efi"" </back>
deactivate dhcp
pxe -> tftp : TFTP Get\n* ""ipxe.efi""
activate tftp #264653
tftp -> pxe : TFTP Reply\n* ""ipxe.efi""
deactivate tftp
end

pxe -> ipxe : Load iPXE

deactivate pxe
activate ipxe #E76F51

group Request iPXE Boot Script
ipxe -> dhcp : DHCP Request
activate dhcp #264653
dhcp -> ipxe : DHCP Reply\n* Bootfile Server: ""router""\n* Bootfile Name: <back:#2A9D8F> ""boot.ipxe"" </back>
deactivate dhcp
ipxe -> tftp : TFTP Get\n* ""boot.ipxe""
activate tftp #264653
tftp -> ipxe : TFTP Reply\n* ""boot.ipxe""
note right
boot.ipxe
---
""#!ipxe""
""initrd /initramfs.xz""
""kernel /vmlinuz ...""
end note
deactivate tftp
end

group Request Image
ipxe -> tftp : TFTP Get\n* ""initramfs4.xz""\n* ""vmlinuz""
activate tftp #264653
tftp -> ipxe : TFTP Reply\n* ""initramfs.xz""\n* ""vmlinuz""
deactivate tftp
end

ipxe -> image: Boot Image

deactivate ipxe
activate image

@enduml
```

## Configuration

### Reset to Factory Defaults

To configure the router freshly, it must be reset first.
This can be done via a switch on the back of the router.
More detailed information can be found in the support article
[EdgeRouter - Reset to Factory Defaults](https://help.ui.com/hc/en-us/articles/205202620-EdgeRouter-Reset-to-Factory-Defaults>).
After resetting, a static IP address must be configured on the client, which configures the router.
Any address from the `192.168.1.0/24` subnet can be selected, although the router itself already uses `192.168.1.1`.

### Firmware

Currently the firmware version `v2.0.9-hotfix.6.5574651` is in use.
The latest firmware can be checked and downloaded from the [Ubiquiti Download Portal](https://www.ui.com/download/edgemax/edgerouter-x/er-x).
It is then possible to apply it via the web UI.

- [192.168.1.1](https://192.168.1.1) (initial), [192.168.1.254](https://192.168.1.254) (configured)
- System > Upgrade System Image

### UI Configurations

- [192.168.1.1](https://192.168.1.1) (initial), [192.168.1.254](https://192.168.1.254) (configured)
- Wizards > Setup Wizards > Basic Setup
  - DNS forwarding: `Use fast public DNS servers`
  - LAN ports > Address: `192.168.1.254` / `255.255.255.0`
  - User setup: `Keep existing user`

### CLI Configurations

- Connect to the router via ssh.

  ``` shell
  ssh ubnt@192.168.1.254
  ```

- Download the latest iPXE image.

  ``` shell
  sudo mkdir -p /var/lib/tftpboot
  sudo curl -s http://boot.ipxe.org/ipxe.efi -o /var/lib/tftpboot/ipxe.efi
  sudo install -m 666 /dev/null /var/lib/tftpboot/boot.ipxe
  ```

- Download the latest talos linux image.

  ``` shell
  TALOS_VERSION="v1.4.5"
  BASE_URL="https://github.com/siderolabs/talos/releases/download/${TALOS_VERSION}"
  sudo curl -sL "${BASE_URL}/initramfs-amd64.xz" -o /var/lib/tftpboot/initramfs-amd64.xz
  sudo curl -sL "${BASE_URL}/vmlinuz-amd64" -o /var/lib/tftpboot/vmlinuz-amd64
  ```

- Configure the router.

  ``` shell
  configure
  ```

  <!--
  ``` shell
  # # Already configured by the basic setup and just for documentation purpose
  # set service dhcp-server disabled false
  # set service dhcp-server hostfile-update disable
  # set service dhcp-server shared-network-name LAN authoritative enable
  # set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 default-router 192.168.1.254
  # set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 dns-server 192.168.1.254
  # set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 lease 86400
  # set service dhcp-server static-arp disable
  # set service dns forwarding listen-on switch0
  ```
  -->

  ``` shell
  # Configure DHCP server settings
  delete service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 start
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 start 192.168.1.51 stop 192.168.1.100
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 domain-name case.local

  # Replace ISC DHCP with Dnsmasq to add DNS and PXE/TFTP server capabilities
  set service dhcp-server use-dnsmasq enable

  # Extend DNS cache size for better performance (default is 150)
  set service dns forwarding cache-size 1000

  # Use fast public DNS servers to forward requests
  set service dns forwarding name-server 8.8.8.8
  set service dns forwarding name-server 1.1.1.1
  set service dns forwarding name-server 9.9.9.9

  # Enable PXE/TFTP
  set service dns forwarding options "enable-tftp"
  set service dns forwarding options "tftp-root=/var/lib/tftpboot"

  # Configure iPXE - chainload boot script
  set service dns forwarding options "dhcp-boot=ipxe.efi"
  set service dns forwarding options "dhcp-userclass=set:ipxe,iPXE"
  set service dns forwarding options "dhcp-boot=tag:ipxe,boot.ipxe"

  # Disable proxy DHCP (175 for iPXE; 176 for no-pxedhcp; 1b for 1 byte)
  set service dns forwarding options "dhcp-option=encap:175, 176, 1b"

  # Disable UNMS
  set service unms disable

  # Configure static host mapping
  set system static-host-mapping host-name router.case.local inet 192.168.1.254
  set system static-host-mapping host-name router.case.local alias router
  set system static-host-mapping host-name kube.case.local inet 192.168.1.101
  set system static-host-mapping host-name kube.case.local alias kube

  # Allow the router to resolve the FQDNs of hosts using just the shortnames
  set system domain-name case.local
  ```

  <details>
  <summary>cloudkoffer-v1 specific configuration</summary>

  ``` shell
  # DHCP - static host mappings
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-1 ip-address 192.168.1.1
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-1 mac-address f4:4d:30:60:70:42
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-2 ip-address 192.168.1.2
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-2 mac-address f4:4d:30:60:68:db
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-3 ip-address 192.168.1.3
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-3 mac-address f4:4d:30:60:6c:9c
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-4 ip-address 192.168.1.4
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-4 mac-address f4:4d:30:60:6d:0d
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-5 ip-address 192.168.1.5
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-5 mac-address f4:4d:30:60:70:62
  ```

  </details><details>
  <summary>cloudkoffer-v2 specific configuration</summary>

  ``` shell
  # DHCP - static host mappings
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-1 ip-address 192.168.1.1
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-1 mac-address 00:1f:c6:9c:1c:a0
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-2 ip-address 192.168.1.2
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-2 mac-address 00:1f:c6:9c:1a:b0
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-3 ip-address 192.168.1.3
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-3 mac-address 00:1f:c6:9c:1c:fe
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-4 ip-address 192.168.1.4
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-4 mac-address 00:1f:c6:9c:1a:ac
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-5 ip-address 192.168.1.5
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-5 mac-address 00:1f:c6:9c:1a:ae
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-6 ip-address 192.168.1.6
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-6 mac-address 00:1f:c6:9c:1c:60
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-7 ip-address 192.168.1.7
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-7 mac-address 00:1f:c6:9c:1a:b1
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-8 ip-address 192.168.1.8
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-8 mac-address 00:1f:c6:9c:1c:64
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-9 ip-address 192.168.1.9
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-9 mac-address 00:1f:c6:9c:1c:8d
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-10 ip-address 192.168.1.10
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-10 mac-address 00:1f:c6:9c:1c:5a
  ```

  </details><details>
  <summary>cloudkoffer-v3 specific configuration</summary>

  ``` shell
  # DHCP - static host mappings
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping switch ip-address 192.168.1.253
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping switch mac-address a0:04:60:08:f7:9f
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-1 ip-address 192.168.1.1
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-1 mac-address 00:1f:c6:9c:8b:2d
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-2 ip-address 192.168.1.2
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-2 mac-address 00:1f:c6:9c:86:8f
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-3 ip-address 192.168.1.3
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-3 mac-address 00:1f:c6:9d:09:08
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-4 ip-address 192.168.1.4
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-4 mac-address 00:1f:c6:9c:8a:20
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-5 ip-address 192.168.1.5
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-5 mac-address 00:1f:c6:9c:89:ca
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-6 ip-address 192.168.1.6
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-6 mac-address 00:1f:c6:9c:90:a7
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-7 ip-address 192.168.1.7
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-7 mac-address 00:1f:c6:9c:89:d7
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-8 ip-address 192.168.1.8
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-8 mac-address 00:1f:c6:9c:8f:f2
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-9 ip-address 192.168.1.9
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-9 mac-address 00:1f:c6:9c:92:a0
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-10 ip-address 192.168.1.10
  set service dhcp-server shared-network-name LAN subnet 192.168.1.0/24 static-mapping node-10 mac-address 00:1f:c6:9c:91:e6
  ```

  </details>

- Persist the configuration and reboot.

  ``` shell
  commit ; save
  exit
  reboot
  ```

- Copy local iPXE config to router.

  ``` shell
  scp boot.ipxe ubnt@192.168.1.254:/var/lib/tftpboot/
  ```

Afterwards, the dhcp, dns and tftp server is active and the configured ip address on the client configuring the router can be removed.

## Maintenance

- Upgrade Talos `vmlinux` and `initramfs`.

  ``` shell
  ssh ubnt@192.168.1.254
  ```

  ``` shell
  TALOS_VERSION="v1.4.5"
  BASE_URL="https://github.com/siderolabs/talos/releases/download/${TALOS_VERSION}"
  sudo curl -sL "${BASE_URL}/initramfs-amd64.xz" -o /var/lib/tftpboot/initramfs-amd64.xz
  sudo curl -sL "${BASE_URL}/vmlinuz-amd64" -o /var/lib/tftpboot/vmlinuz-amd64
  ```

- Switch default boot menu entry.

  - Connect to router.

    ``` shell
    ssh ubnt@192.168.1.254
    sudo su -
    ```

  - Open file `boot.ipxe`.

    ``` shell
    vi /var/lib/tftpboot/boot.ipxe
    ```

  - Switch menu default to `talos` or `reset` (line 4).

    ``` text
    # Boot into talos operating system.
    isset ${menu-default} || set menu-default talos

    # Wipe disk and boot into talos maintenance mode.
    isset ${menu-default} || set menu-default reset
    ```

  - Exit from router.

    ``` shell
    exit
    exit
    ```

- Show, export and diff active configuration

  ``` shell
  ssh ubnt@192.168.1.254
  ```

  ``` shell
  show configuration | no-more > config.boot
  exit
  ```

  ``` shell
  CLOUDKOFFER="v3" # v1, v2, v3
  CLUSTER_NAME="talos-cloudkoffer-${CLOUDKOFFER}"

  scp ubnt@192.168.1.254:config.boot config/tmp.boot
  diff -u "config/${CLUSTER_NAME}.boot" config/tmp.boot
  ```

## Useful links

- [EdgeRouter - DNS Forwarding Setup and Options](https://help.ui.com/hc/en-us/articles/115010913367)
- [EdgeRouter - DHCP Server Using Dnsmasq](https://help.ui.com/hc/en-us/articles/115002673188)
