<!-- markdownlint-disable MD033 -->
# Talos / Hardware / Client

## Installation

- Install Homebrew.

  ``` shell
  bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

  (echo 'eval "$(/usr/local/bin/brew shellenv)"') >> ~/.zprofile
  eval "$(/usr/local/bin/brew shellenv)"
  ```

- Install Homebrew Packages.

  <details>
  <summary>Click to expand steps ...</summary>

  ``` shell
  brew install \
    age \
    bat \
    checkov \
    cilium-cli \
    colima \
    curl \
    doitlive \
    fluxcd/tap/flux \
    fzf \
    gettext \
    gh \
    git \
    weaveworks/tap/gitops \
    glab \
    go \
    hadolint \
    helm \
    norwoodj/tap/helm-docs \
    jq \
    jsonschema \
    k9s \
    kdash-rs/kdash/kdash \
    krew \
    kube-score \
    kubeconform \
    kubernetes-cli \
    kubeseal \
    kubeval \
    kustomize \
    minio/stable/mc \
    pre-commit \
    sdkman/tap/sdkman-cli \
    shellcheck \
    sops \
    step \
    stern \
    terraform \
    terraform-docs \
    tflint \
    tfsec \
    trivy \
    watch \
    wget \
    yamllint \
    yq \
    zsh
  ```

  </details>

- Install Homebrew Casks.

  <details>
  <summary>Click to expand steps ...</summary>

  ``` shell
  brew install --cask \
    1password \
    1password-cli \
    balenaetcher \
    caffeine \
    firefox \
    google-chrome \
    google-cloud-sdk \
    intellij-idea \
    iterm2 \
    keepassxc \
    lens \
    macfuse \
    postman \
    rectangle \
    rapidapi \
    slack \
    visual-studio-code \
    zoom
  ```

  </details>

- Install Homebrew Casks (quarantine).

  <details>
  <summary>Click to expand steps ...</summary>

  ``` shell
  brew install --cask --no-quarantine \
    siderolabs/talos/talosctl
  ```

  </details>

- Install Oh-My-ZSH.

  ``` shell
  sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
  ```

<!--
- Install kubectl plugins.

  ``` shell
  kubectl krew install directpv
  ```
-->

## Configuration

- Configure environment variables.

  ``` shell
  CLOUDKOFFER="v3" # v1, v2, v3
  CLUSTER_NAME="talos-cloudkoffer-${CLOUDKOFFER}"
  ```

- Configure shell profile.

  ``` shell
  # sdkman
  cat <<'EOT' >> ~/.zprofile
  export SDKMAN_DIR=$(brew --prefix sdkman-cli)/libexec
  [[ -s "${SDKMAN_DIR}/bin/sdkman-init.sh" ]] && source "${SDKMAN_DIR}/bin/sdkman-init.sh"
  EOT

- Clone `talos` GitLab repository.

  ``` shell
  cd ~/Documents
  TOKEN="$(op read "op://QAware-Showcases/GitLab - Talos - Project Access Token/password" --account=qawaregmbh)"
  git clone "https://Automation:${TOKEN}@gitlab.com/qaware/internal/projects/sam/showcases/cloudkoffer/talos.git"
  ```

- Clone `cloud-native-explab` GitHub repository.

  ``` shell
  cd ~/Documents/talos/deployment-stack/
  git clone https://github.com/qaware/cloud-native-explab
  cd cloud-native-explab
  git switch cloudkoffer 2>/dev/null || git switch -c cloudkoffer
  ```

- Clone `kubepad` GitHub repository.

  ``` shell
  cd ~/Documents
  git clone https://github.com/qaware/kubepad.git
  ```

- Clone `cloudcontrol` GitHub repository.

  ``` shell
  cd ~/Documents
  git clone https://github.com/qaware/cloudcontrol.git
  ```

- Configure 1Password CLI.

  - Sign in with desktop app.

    - Open and unlock the 1Password app.
    - Click your account or collection at the top of the sidebar.
    - Navigate to Settings > Developer.
    - Select `Connect with 1Password CLI`.

  - Sign in manually.

    ``` shell
    eval $(op account add --address=qawaregmbh.1password.eu --signin)
    ```

    > **Hint**: Session tokens expire after 30 minutes of inactivity, after which you’ll need to sign in again and save a new token.

    ``` shell
    eval $(op signin --account=qawaregmbh)
    ```

- Fetch `talosconfig`.

  ``` shell
  cd ~/Documents/talos/deployment-talos/terraform

  GITLAB_USER="$(op read "op://QAware-Showcases/GitLab - Talos - Project Access Token/username" --account=qawaregmbh)"
  GITLAB_TOKEN="$(op read "op://QAware-Showcases/GitLab - Talos - Project Access Token/password" --account=qawaregmbh)"

  terraform init \
    -upgrade \
    -backend-config="address=https://gitlab.com/api/v4/projects/43783075/terraform/state/${CLUSTER_NAME}" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/43783075/terraform/state/${CLUSTER_NAME}/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/43783075/terraform/state/${CLUSTER_NAME}/lock" \
    -backend-config="username=${GITLAB_USER}" \
    -backend-config="password=${GITLAB_TOKEN}" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"

  terraform output -raw talosconfig > talosconfig
  talosctl config node 192.168.1.1 --talosconfig=talosconfig
  talosctl config merge talosconfig
  rm -rf talosconfig
  talosctl config use-context "${CLUSTER_NAME}"
  ```

- Fetch `kubeconfig`.

  ``` shell
  talosctl kubeconfig
  kubectl config use-context "admin@${CLUSTER_NAME}"
  ```

## Operation

- Configure environment variables.

  ``` shell
  CLOUDKOFFER="v3" # v1, v2, v3
  CLUSTER_NAME="talos-cloudkoffer-${CLOUDKOFFER}"

  case "${CLOUDKOFFER}" in
    v1) NUMBER_OF_NODES=5 ;;
    v2) NUMBER_OF_NODES=10 ;;
    v3) NUMBER_OF_NODES=10 ;;
  esac
  ```

- Shutdown Talos cluster (without cordon/drain).

  ``` shell
  for i in {1..${NUMBER_OF_NODES}}; do
    talosctl shutdown \
      --force=true \
      --wait=false \
      --nodes="192.168.1.${i}"
  done
  ```

- Cordon, drain and shutdown single Talos node.

  ``` shell
  talosctl shutdown --nodes=192.168.1.x
  ```

- Reset Talos cluster.

  > **Danger**: This is a destructive action and cannot be undone.

  ``` shell
  for i in {1..${NUMBER_OF_NODES}}; do
    talosctl reset \
      --graceful=false \
      --wait=false \
      --reboot=true \
      --nodes="192.168.1.${i}"
  done
  ```

<!--
- Manage DirectPV disks

  ``` shell
  kubectl directpv drives ls
  kubectl directpv drives format --drives=/dev/nvme1n1 --nodes=node-1,node-2,node-3
  ```
-->
