# Talos / Hardware / Switch

## Cloudkoffer v1

![TP-Link TL-SG108](img/TL-SG108.jpg "TP-Link TL-SG108")

- **Type**: TP-Link TL-SG108 (unmanaged)

## Cloudkoffer v2

![Netgear GS316](img/GS316.jpg "Netgear GS316")

- **Type**: Netgear GS316 (unmanaged)

## Cloudkoffer v3

![Netgear ProSafe GS116Ev2](img/GS116Ev2.jpg "Netgear ProSafe GS116Ev2")

- **Type**: Netgear ProSafe GS116Ev2 (managed)
- **URL**: [192.168.1.253](https://192.168.1.253)
- **Password**
  - Initial: `password`
  - Configured: `Password1`

### Reset

To reset the switch to factory default, push and hold the reset button (Factory Defaults) on the front of the switch for several seconds.

### Firmware

Currently the firmware version `V2.6.0.48` is in use.
The latest firmware can be checked and downloaded from the [Netgear Download Portal](https://www.netgear.de/support/product/gs116ev2.aspx#download).
It is then possible to apply it via the web UI.

- [192.168.1.253](https://192.168.1.253)
- System > Upgrade System Image

### Configuration

- [192.168.1.253](https://192.168.1.253)
- System > Management
  - Loop Detection > Loop Detection: `Enable`
  - Switch Management Mode > Switch Management Mode > `Web browser and Plus Utility`

### Useful links

- [How do I reset a smart switch to factory default settings?](https://kb.netgear.com/24679/How-do-I-reset-a-smart-switch-to-factory-default-settings)
- [What is the default IP address of my NETGEAR Smart Managed Plus, Smart Managed Pro, or Insight Managed Smart Cloud switch?](https://kb.netgear.com/30418/What-is-the-default-IP-address-of-my-NETGEAR-Smart-Managed-Plus-Smart-Managed-Pro-or-Insight-Managed-Smart-Cloud-switch)
