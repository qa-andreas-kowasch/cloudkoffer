# Talos / Hardware / Case

## Cloudkoffer v1

## Cloudkoffer v2

![Cloudkoffer v2 - 1](img/cloudkoffer-v2-1.jpg "Cloudkoffer v2 - 1")
![Cloudkoffer v2 - 2](img/cloudkoffer-v2-2.jpg "Cloudkoffer v2 - 2")
![Cloudkoffer v2 - 3](img/cloudkoffer-v2-3.jpg "Cloudkoffer v2 - 3")
![Cloudkoffer v2 - 4](img/cloudkoffer-v2-4.jpg "Cloudkoffer v2 - 4")
![Cloudkoffer v2 - 5](img/cloudkoffer-v2-5.jpg "Cloudkoffer v2 - 5")

## Cloudkoffer v3

![Cloudkoffer v3 - 1](img/cloudkoffer-v3-1.jpg "Cloudkoffer v3 - 1")
![Cloudkoffer v3 - 2](img/cloudkoffer-v3-2.jpg "Cloudkoffer v3 - 2")
![Cloudkoffer v3 - 3](img/cloudkoffer-v3-3.jpg "Cloudkoffer v3 - 3")
![Cloudkoffer v3 - 4](img/cloudkoffer-v3-4.jpg "Cloudkoffer v3 - 4")
![Cloudkoffer v3 - 5](img/cloudkoffer-v3-5.jpg "Cloudkoffer v3 - 5")
![Cloudkoffer v3 - 6](img/cloudkoffer-v3-6.jpg "Cloudkoffer v3 - 6")
![Cloudkoffer v3 - 7](img/cloudkoffer-v3-7.jpg "Cloudkoffer v3 - 7")
![Cloudkoffer v3 - 8](img/cloudkoffer-v3-8.jpg "Cloudkoffer v3 - 8")
