<!-- markdownlint-disable MD033 -->
# Talos / Hardware / Node

Name | Cloudkoffer v1 | Cloudkoffer v2 | Cloudkoffer v3
--- | --- | --- | ---
| | ![NUC6i3SYB](img/NUC6i3SYB.jpg "NUC6i3SYB") | ![NUC6i7KYB](img/NUC6i7KYB.jpg "NUC6i7KYB") | ![NUC6i7KYB](img/NUC6i7KYB.jpg "NUC6i7KYB")
Type | 5x [Intel NUC Kit NUC6i3SYB](https://ark.intel.com/content/www/us/en/ark/products/89186/intel-nuc-kit-nuc6i3syk.html) ([Specs](https://www.intel.com/content/dam/support/us/en/documents/boardsandkits/NUC6i5SYB_NUC6i3SYB_TechProdSpec.pdf)) | 10x [Intel NUC Kit NUC6i7KYB](https://ark.intel.com/content/www/us/en/ark/products/89187/intel-nuc-kit-nuc6i7kyk.html) ([Specs](https://www.intel.com/content/dam/support/us/en/documents/boardsandkits/NUC6i7KYK_TechProdSpec.pdf)) | 10x [Intel NUC Kit NUC6i7KYB](https://ark.intel.com/content/www/us/en/ark/products/89187/intel-nuc-kit-nuc6i7kyk.html) ([Specs](https://www.intel.com/content/dam/support/us/en/documents/boardsandkits/NUC6i7KYK_TechProdSpec.pdf))
UEFI | [SYSKLi35.86A.0073.2020.0909.1625](https://www.intel.com/content/www/us/en/download/18449/bios-update-syskli35.html) | [KYSKLi70.86A.0074.2021.1029.0102](https://www.intel.com/content/www/us/en/download/18677/bios-update-kyskli70.html) | [KYSKLi70.86A.0074.2021.1029.0102](https://www.intel.com/content/www/us/en/download/18677/bios-update-kyskli70.html)
Processor | Intel Core i3-6100U<br />- Base Frequency: 2.30 GHz<br />- Cache: 3 MB (L3)<br />- Cores/Threads: 2/4 | Intel Core i7-6770HQ<br />- Base Frequency: 2.60 GHz<br />- Turbo Frequency: 3.50 GHz<br />- Cache: 6 MB (L3), 128 MB (L4)<br />- Cores/Threads: 4/8 | Intel Core i7-6770HQ<br />- Base Frequency: 2.60 GHz<br />- Turbo Frequency: 3.50 GHz<br />- Cache: 6 MB (L3), 128 MB (L4)<br />- Cores/Threads: 4/8
Memory | 32 GB - 2x 16 GB (DDR4-2133) | 32 GB - 2x 16 GB (DDR4-2133) | 32 GB - 2x 16 GB (DDR4-2133)
Graphics | Intel Iris Graphics 520<br />- Mini-DP 1.2<br />- HDMI 1.4b | Intel Iris Pro Graphics 580<br />- Mini-DP 1.2<br />- HDMI 2.0<br />- USB-C (DP1.2) | Intel Iris Pro Graphics 580<br />- Mini-DP 1.2<br />- HDMI 2.0<br />- USB-C (DP1.2)
Storage | - Samsung SSD 850 Evo 250 GB | - ???<br />- ??? | - Samsung SSD 960 PRO 1 TB
Networking | - Intel Ethernet Connection I219-V<br />- Intel Wireless-AC 8260 + Bluetooth 4.2 | - Intel Ethernet Connection I219-LM<br />- Intel Wireless-AC 8260 + Bluetooth 4.2 | - Intel Ethernet Connection I219-LM<br />- Intel Wireless-AC 8260 + Bluetooth 4.2
TDP | 15 W | 45 W | 45 W
Features | - SDXC (with UHS-I support) | - Thunderbolt 3 (40Gbps)<br />- USB 3.1 Gen 2 (10Gbps)<br />- SDXC (with UHS-I support) | - Thunderbolt 3 (40Gbps)<br />- USB 3.1 Gen 2 (10Gbps)<br />- SDXC (with UHS-I support)

## MAC Addresses

Name | Cloudkoffer v1 | Cloudkoffer v2 | Cloudkoffer v3
--- | --- | --- | ---
Node 1 | f4:4d:30:60:70:42 | 00:1f:c6:9c:1c:a0 | 00:1f:c6:9c:8b:2d
Node 2 | f4:4d:30:60:68:db | 00:1f:c6:9c:1a:b0 | 00:1f:c6:9c:86:8f
Node 3 | f4:4d:30:60:6c:9c | 00:1f:c6:9c:1c:fe | 00:1f:c6:9d:09:08
Node 4 | f4:4d:30:60:6d:0d | 00:1f:c6:9c:1a:ac | 00:1f:c6:9c:8a:20
Node 5 | f4:4d:30:60:70:62 | 00:1f:c6:9c:1a:ae | 00:1f:c6:9c:89:ca
Node 6 | | 00:1f:c6:9c:1c:60 | 00:1f:c6:9c:90:a7
Node 7 | | 00:1f:c6:9c:1a:b1 | 00:1f:c6:9c:89:d7
Node 8 | | 00:1f:c6:9c:1c:64 | 00:1f:c6:9c:8f:f2
Node 9 | | 00:1f:c6:9c:1c:8d | 00:1f:c6:9c:92:a0
Node 10 | | 00:1f:c6:9c:1c:5a | 00:1f:c6:9c:91:e6

## UEFI

- Advanced
  - Main
    - Default Visual BIOS Start Page > `Home Page 1`
    - Event Log
      - Clear Event Log > `unchecked`
      - Event Logging > `checked`
  - Devices
    - USB
      - USB Configuration
        - USB Legacy > `checked`
        - Portable Device Charging Mode > `Charging in S3/S4/S5`
      - USB Ports
        - USB Port 0x: `Enable`
    - SATA
      - Chipset SATA Controller Configuration
        - Chipset SATA > `checked`
        - Chipset SATA Mode > `AHCI`
        - S.M.A.R.T > `checked`
        - SATA Port > `checked` (only cloudkoffer-v1)
        - Hard Disk Pre-Delay > `0`
        - HDD Activity LED > `checked`
        - M.2 PCIe SSD LED > `checked` (only cloudkoffer-v2/v3)
    - Video
      - Video Configuration
        - IDG Minimum Memory > `64 MB`
        - IDG Aperture Size > `256 MB`
        - IDG Primary Video Port > `Auto`
    - Onboard Devices
      - Onboard Device Configuration
        - Audio > `unchecked`
        - LAN > `checked`
        - WLAN > `unchecked`
        - Bluetooth > `unchecked`
        - SD Card > `read/write`
      - Legacy Device Configuration
        - Enhance Consumer IR > `unchecked`
        - High Precision Event Timers > `checked`
        - Num Lock > `Checked`
    - PCI
      - PCI Configuration
        - PCI Latency Timer > `32` (only cloudkoffer-v1)
        - M.2 Slot > `checked` (only cloudkoffer-v1)
        - M.2 Slot 1 > `checked` (only cloudkoffer-v2/v3)
        - M.2 Slot 2 > `checked` (only cloudkoffer-v2/v3)
  - Cooling
    - CPU Fan Header
      - Fan Control Mode > `Cool`
      - Minimum Duty Cycle (%) > `35` (cloudkoffer-v1), `27` (cloudkoffer-v2/v3)
      - Primary Temperature Sensor > `Processor`
      - Minimum Temperature (°C) > `71` (cloudkoffer-v1), `65` (cloudkoffer-v2/v3)
      - Duty Cycle Increment (%/°C) > `2`
      - Secondary Temperature Sensor > `Memory` (only cloudkoffer-v1)
      - Minimum Temperature (°C) > `68` (only cloudkoffer-v1)
      - Duty Cycle Increment (%/°C) > `2` (only cloudkoffer-v1)
  - Performance
    - Processor
      - Core Settings
        - Intel Hyper-Threading Technology > `checked`
        - Intel Tubrbo Boost Technology > `checked` (only cloudkoffer-v2/v3)
        - Active Processor Cores > `ALL`
        - Real-Time Performance Tuning > `checked`
        - Silicon Debug Features > `unchecked`
    - Memory
      - General Settings
        - Memory Profiles > `Automatic`
        - Round Trip Latency > `checked`
        - TCR > `Disabled`
  - Security
    - Password
      - Supervisor Password > `Not installed`
      - User Password > `Not installed`
      - Master Hard Disk Drive Password > `Not installed` (only cloudkoffer-v1)
      - Hard Disk Drive Password > `Not installed` (only cloudkoffer-v1)
      - Hard Disk Drive Password Prompt > `checked` (only cloudkoffer-v1)
    - Security Features
      - Allow UEFI 3rd party driver loaded > `unchecked`
      - Unattended BIOS Configuration > `Always Prompt`
      - Execute Disable Bit > `checked`
      - Intel Virtualization Technology > `checked`
      - Intel VT for Directed I/O (VT-d) > `checked`
      - Fixed Disk Boot Selector > `Normal`
      - Intel Platform Trust Technology > `unchecked`
      - Intel Software Guard Extenstion (SGX) > `Software Controlled`
      - SGX Owner EPOCHs > `Factory Default`
      - Thunderbolt Security Level > `Unique ID` (only cloudkoffer-v2/v3)
  - Power
    - Primary Power Settings
      - Balanced Enabled > `unchecked`
      - Low Power Enabled > `unchecked`
      - Max Performance Enabled > `checked`
    - Secondary Power Settings
      - Power Sense > `checked`
      - After Power Failure > `Stay Off`
      - Deep S4/S5 > `unchecked`
      - S0 State Indicator > `ON (solid, primary color)`
      - S3 State Indicator > `Blink (alternate color @ 0.25 Hz)`
      - Wake on LAN from S4/S5 > `Power On - PXE Boot`
      - Wake System from S5 > `unchecked`
      - USB S4/S5 Power > `unchecked`
      - PCIe ASPM Power > `checked`
      - Native ACPI OS PCIe Supprt > `unchecked` (cloudkoffer-v1), `checked` (cloudkoffer-v2/v3)
      - Flash Update Sleep Delay > `unchecked`
  - Boot
    - Boot Priority
      - UEFI Boot Priority
        - UEFI Boot > `checked`
      - Legacy Boot Priority
        - Legacy Boot > `unchecked`
    - Boot Configuration
      - Boot UEFI
        - Fast Boot > `unchecked`
        - Boot USB Devices First > `checked`
        - Boot Network Devices Last `checked`
        - Unlimited Boot to Network Attempts > `unchecked`
        - BIOS Setup Auto-Entry > `unchecked`
      - Boot Devices
        - Internal UEFI Shell > `unchecked`
        - USB > `checked`
        - Optical > `checked`
        - Network Boot > `UEFI PXE & iSCSI`
      - Boot Display Config
        - Failsafe Watchdog > `checked`
        - Suppress Alert Messages At Boot > `unchecked`
        - Expansion Card Test > `Disable`
        - Keyboard Ready Beep > `unchecked`
        - POST Function Hotkeys Displayed > `checked`
        - Display F2 to Enter Setup > `checked`
        - Display F7 to Update BIOS > `checked`
        - Display F10 to Enter Boot Menu > `checked`
        - Display F12 for Network Boot > `checked`
    - Secure Boot
      - Secure Boot Config
        - Securte Boot > `unchecked`
